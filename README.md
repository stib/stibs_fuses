# stibs_fuses

Fuses for DaVinci Resolve and Fusion. I'm learning by doing, so these are free as in speech, and as in beer, and as in solo: use at your own risk.

Discussion about release 0.1, the white noise fuse, is over on WeSuckLess https://www.steakunderwater.com/wesuckless/viewtopic.php?p=46798

## Installation

Just download the zip, unzip it and yeet everything into the fuses folder:


### for Linux

fuses are installed at:

`~/.local/share/DaVinciResolve/Fusion/Fuses/` where `~/` is your home folder. the `.local` folder is normally invisible

### for Mac

fuses are installed at:

`~/Library/Application Support/Blackmagic Design/DaVinci Resolve/Support/Fusion/Fuses` where `~/` is your home folder

### for Windows

fuses are installed at: 

`C:\ProgramData\Blackmagic Design\DaVinci Resolve\Support\Fusion\Fuses`

